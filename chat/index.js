var app = require('express')();
var http = require('http').Server(app);
/****** Serving Static pages  *********/
//app.use(express.static('public'));
/*
 *Integrating Socket.IO
 */
var io = require('socket.io')(http);

/*
 *first step
 */
//app.get('/', function(req, res){
  //res.send('<h1>Hello world</h1>');
//});

/*
 *Serving HTML - second step
 */
app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

/*
 *Integrating Socket.IO - third step
 */
//app.get('/', function(req, res){ // did not work used previous snippet
  //res.sendfile('index.html');
//});
//io.on('connection', function(socket){
  //console.log('a user connected');
  //socket.on('disconnect', function(){
	//console.log('user disconnected');
  //});
//});

/*
 *Emitting events - fourth step
 */
io.on('connection', function(socket){
  socket.on('chat message', function(msg){
	console.log('message: ' , msg);
	socket.broadcast.emit('hi', 'this is just for you');
	io.emit('chat message', msg);
  });
});

http.listen(3001, function(){
  console.log('listening on *:3001');
});

